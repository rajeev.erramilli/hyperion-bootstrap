{-# LANGUAGE RecordWildCards #-}

module Control.Concurrent.TrackedVar where

import           Control.Concurrent.STM
import           Control.Monad          (void)

data TrackedVar a = TrackedVar
  { itemVar  :: TMVar a
  , freshVar :: TMVar ()
  }

newTrackedVar :: a -> IO (TrackedVar a)
newTrackedVar a = do
  itemVar  <- newTMVarIO a
  freshVar <- newTMVarIO ()
  return TrackedVar{..}

readTrackedVar :: TrackedVar a -> IO a
readTrackedVar TrackedVar{..} = atomically $ do
  void $ tryTakeTMVar freshVar
  readTMVar itemVar

readSilentTrackedVar :: TrackedVar a -> IO a
readSilentTrackedVar TrackedVar{..} = atomically (readTMVar itemVar)

waitTrackedVar :: TrackedVar a -> IO ()
waitTrackedVar TrackedVar{..} = atomically (readTMVar freshVar)

modifyTrackedVar :: TrackedVar a -> (a -> a) -> IO ()
modifyTrackedVar TrackedVar{..} f = atomically $ do
  a <- takeTMVar itemVar
  putTMVar itemVar (f a)
  void $ tryPutTMVar freshVar ()

modifySilentTrackedVar :: TrackedVar a -> (a -> a) -> IO ()
modifySilentTrackedVar TrackedVar{..} f = atomically $ do
  a <- takeTMVar itemVar
  putTMVar itemVar (f a)
