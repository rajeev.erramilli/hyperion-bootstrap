{-# LANGUAGE LambdaCase #-}

module Control.Concurrent.Memoize where

import           Control.Concurrent.STM (TMVar, atomically, modifyTVar,
                                         newEmptyTMVar, newTVarIO, putTMVar,
                                         readTMVar, readTVar)
import           Control.Monad.IO.Class
import           Data.Map.Strict        (Map)
import qualified Data.Map.Strict        as Map

-- Memoize an IO computation by storing a Map of MVar's
-- containing results.  Ensures that the function f is run at most
-- once for each argument.
memoize :: (Ord t, MonadIO m) => (t -> m a) -> IO (t -> m a)
memoize f = do
  cache <- newTVarIO (Map.empty :: Map a (TMVar b))
  let lookupCache a =
        Map.lookup a <$> readTVar cache >>= \case
          -- Computation never run before, make a new TMVar for result
          Nothing -> do
            v <- newEmptyTMVar
            modifyTVar cache (Map.insert a v)
            return (Left v)
          -- Computation already run, return a TMVar for the result
          Just v  -> return (Right v)
  return $ \a ->
    liftIO (atomically (lookupCache a)) >>= \case
      -- Computation never run before, run it now
      Left v -> do
        x <- f a
        liftIO (atomically (putTMVar v x))
        return x
      -- Computation already run, read TMVar for result
      Right v ->
        liftIO (atomically (readTMVar v))

runOnce :: MonadIO m => m a -> m (m a)
runOnce f = liftIO $ do
  f' <- memoize (\() -> f)
  return (f' ())
