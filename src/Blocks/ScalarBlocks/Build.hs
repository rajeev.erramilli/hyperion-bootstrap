{-# LANGUAGE FlexibleContexts  #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE StaticPointers    #-}
{-# LANGUAGE TupleSections     #-}
{-# LANGUAGE TypeFamilies      #-}

module Blocks.ScalarBlocks.Build where

import qualified Blocks.ScalarBlocks            as SB
import           Bootstrap.Build                (BuildLink (..))
import qualified Bootstrap.Math.DampedRational  as DR
import           Control.Distributed.Process    (Process)
import           Control.Monad                  (filterM)
import           Control.Monad.IO.Class         (liftIO)
import           Control.Monad.Reader           (asks, local)
import           Data.List                      (maximumBy)
import           Data.List.NonEmpty             (groupAllWith)
import           Data.Ord                       (comparing)
import           Data.Void                      (Void)
import           Hyperion                       (Job, cAp, cPure, jobNodeCpus,
                                                 mapConcurrently_, ptrAp,
                                                 remoteEval, setTaskCpus)
import           Hyperion.Bootstrap.Bound.Types (BoundConfig (..), BoundFiles,
                                                 SDPFetchValue, blockDir)
import           Hyperion.LockMap               (withLocks)
import qualified Hyperion.Log                   as Log
import           Hyperion.WorkerCpuPool         (NumCPUs (..))
import           System.Directory               (doesFileExist)
import           System.FilePath.Posix          ((</>))
import qualified Data.Set as Set

type ScalarBlockDR a = DR.DampedRational (SB.FourRhoCrossing a) SB.DerivMap a

type instance SDPFetchValue a SB.BlockTableKey = ScalarBlockDR a

maximumByMaybe :: (a -> a -> Ordering) -> [a] -> Maybe a
maximumByMaybe _ [] = Nothing
maximumByMaybe o l  = Just $ maximumBy o l

scalarBlockBuildLink
  :: BoundConfig
  -> BoundFiles
  -> Bool
  -> BuildLink Job SB.BlockTableKey Void
scalarBlockBuildLink config files useLocks = BuildLink
  { buildDeps = const []
  , checkCreated = checkCreated' (blockDir files)
  , buildAll = \tables -> do
    let
      nonSpin blockTable = blockTable { SB.spin = 0 }
      -- We can create all block tables by creating the table with
      -- maximum spin for each value of the other parameters
      maxSpinBlocks = map (maximumBy (comparing SB.spin)) (groupAllWith nonSpin (Set.toList tables))
    nodeCpus <- asks jobNodeCpus
    local (setTaskCpus nodeCpus) $
      mapConcurrently_ (runScalarBlocks nodeCpus) maxSpinBlocks
  }
  where
    checkCreated' dir = \t -> liftIO (doesFileExist (SB.blockTableFilePath dir t))
    scalarBlocksExecutable = scriptsDir config </> "scalar_blocks.sh"
    runScalarBlocks (NumCPUs numThreads) t = do
      Log.info "Building BlockTables" t
      remoteEval $
        static remoteWriteTable `ptrAp`
        cPure scalarBlocksExecutable `cAp`
        cPure numThreads `cAp`
        cPure (blockDir files) `cAp`
        cPure t `cAp`
        cPure useLocks
    remoteWriteTable :: FilePath -> Int -> FilePath -> SB.BlockTableKey -> Bool -> Process ()
    remoteWriteTable scalarBlocksExecutable' numThreads blockTableDir bt useLocks' = if useLocks' then do
        let
          bts = map (\s -> (bt {SB.spin = s})) [0 .. SB.spin bt]
          btsWithPath = map (,blockTableDir) bts
        withLocks btsWithPath $ do
          notcreated <- filterM (fmap not . checkCreated' blockTableDir) bts
          let
            mbt = maximumByMaybe (comparing SB.spin) notcreated
          case mbt of
            Nothing  -> return ()
            Just bt' ->
              liftIO $ SB.writeBlockTable scalarBlocksExecutable' numThreads  blockTableDir bt'
      else liftIO $ SB.writeBlockTable scalarBlocksExecutable' numThreads  blockTableDir bt
