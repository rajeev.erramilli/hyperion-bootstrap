{-# LANGUAGE FlexibleContexts  #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards   #-}
{-# LANGUAGE StaticPointers    #-}
{-# LANGUAGE TypeFamilies      #-}

module Blocks.Blocks3d.Build where

import Blocks (Coordinate (..))
import qualified Blocks.Blocks3d                as B3d
import           Blocks.Blocks3d.Types          (BlockTableKey' (..))
import           Blocks.Sign                    (toNum)
import           Bootstrap.Build                (BuildLink (..))
import           Control.Monad                  (void)
import           Control.Monad.IO.Class         (liftIO)
import           Control.Monad.Reader           (asks, local)
import           Data.BinaryHash                (hashBase64Safe)
import qualified Data.List as List
import qualified Data.Set as Set
import           Data.Void                      (Void)
import           Hyperion                       (Job, cAp, cPure, jobNodeCpus,
                                                 mapConcurrently_, ptrAp,
                                                 remoteEval, setTaskCpus, jobTaskCpus)
import           Hyperion.Bootstrap.Bound.Types (BoundConfig (..), BoundFiles,
                                                 SDPFetchValue, blockDir)
import Bootstrap.Math.HalfInteger (HalfInteger (..), isInteger)
import qualified Hyperion.Log                   as Log
import           Hyperion.WorkerCpuPool         (NumCPUs (..))
import           System.Directory               (doesFileExist)
import           System.FilePath.Posix          ((</>))

type instance SDPFetchValue a B3d.BlockTableKey = B3d.BlockTable a

blockMinusRelativeCost :: BlockTableKey' (Set.Set HalfInteger) -> Integer
blockMinusRelativeCost BlockTableKey{..} =
  - nJs * (l12 * l43 * (l12 + l43) + l12' * l43' * (l12' + l43'))
  where
    nJs = toInteger $ Set.size jInternal
    l halfInt
      | isInteger halfInt = (twice halfInt) `div` 2 
      | otherwise = (twice halfInt + 1) `div` 2
    l' halfInt
      | isInteger halfInt = l halfInt + 1
      | otherwise = l halfInt
    l12 = l j12
    l43 = l j43
    l12' = l' j12
    l43' = l' j43
-- | TODO: Currently, DebugLevel = Debug is turned on by default for
-- writeBlockTables. This could be changed to give the user more
-- control.
block3dBuildLink
  :: BoundConfig
  -> BoundFiles
  -> BuildLink Job B3d.BlockTableKey Void
block3dBuildLink config files = BuildLink
  { buildDeps = const []
  , checkCreated = liftIO . doesFileExist . B3d.blockTableFilePath (blockDir files)
  , buildAll = \keys -> do
      (NumCPUs nodeCpus) <- asks jobNodeCpus
      -- (NumCPUs totalCpus) <- asks jobTaskCpus
      let
        gKeys = List.sortOn blockMinusRelativeCost $ B3d.groupBlockTableKeys keys
        (radKeys,otherKeys) = List.partition isRadialBTK gKeys
        -- -- this is the number of cores we would allocate to each (non-radial) block if a) we had
        -- -- CPUs without any node considerations and b) we wanted to try to build every block
        -- -- all at once.
        -- simCoresPerBlock = (totalCpus - length radKeys) `div` length otherKeys
        -- -- we can then impose the constraint that we can only use a number of cores between
        -- -- 1 and the number of cores in a single node.
        -- coresPerBlock = max 1 $ min nodeCpus simCoresPerBlock
      mapConcurrently_ (runBlocks3d nodeCpus B3d.Debug) $ otherKeys ++ radKeys
  }
  where
    blocks3dExecutable = scriptsDir config </> "blocks_3d.sh"
    runBlocks3d nodeCpus debugLevel t = local (setTaskCpus (NumCPUs cpus)) $ do
      Log.info "Building Blocks" (t, hashBase64Safe (void t))
      remoteEval $ ptrAp (static liftIO) $
        static B3d.writeBlockTables `ptrAp`
        cPure blocks3dExecutable `cAp`
        cPure cpus `cAp`
        cPure debugLevel `cAp`
        cPure (blockDir files) `cAp`
        cPure t
      where
        cpus
          | isRadialBTK t = 1
          | otherwise = numThreads nodeCpus t
    -- We want to make sure that blocks_3d only gets as many threads as it needs, which for now is approximately equal to nmax.
    -- numThreads _ _ = 1
    isRadial :: Coordinate -> Bool
    isRadial XT_Radial = True
    isRadial WS_Radial = True
    isRadial _ = False
    isRadialBTK :: BlockTableKey' (Set.Set HalfInteger) -> Bool
    isRadialBTK BlockTableKey{coordinates=coordinates} = Set.null others
      where
        (_,others) = Set.partition isRadial coordinates
    cpuMax cpus BlockTableKey{..}
      | mod lambda 2 == 1 = min ((lambda+1) `div ` 2) cpus
      | otherwise = min ((lambda + 1 + toNum fourPtSign) `div` 2) cpus
    -- since I don't know the exact performance characteristics with numbers of CPUs, I'm
    -- going to approximate and say we need half as many cpus as we could use
    numThreads cpus k
      | mod c 2 == 1 = c `div` 2 + 1
      | otherwise = max 1 (c `div` 2)
      where
        c = cpuMax cpus k
