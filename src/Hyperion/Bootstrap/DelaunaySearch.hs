{-# LANGUAGE DataKinds           #-}
{-# LANGUAGE FlexibleContexts    #-}
{-# LANGUAGE LambdaCase          #-}
{-# LANGUAGE MultiWayIf          #-}
{-# LANGUAGE OverloadedStrings   #-}
{-# LANGUAGE PolyKinds           #-}
{-# LANGUAGE RecordWildCards     #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeApplications    #-}
{-# LANGUAGE TypeInType          #-}

module Hyperion.Bootstrap.DelaunaySearch where

import           Control.Applicative                 (empty, (<|>))
import           Control.Arrow                       (second)
import           Control.Distributed.Process         (Process)
import           Control.Monad.Base                  (MonadBase)
import           Control.Monad.Catch                 (MonadCatch)
import           Control.Monad.IO.Class              (MonadIO)
import           Control.Monad.Reader                (MonadReader)
import           Control.Monad.Trans.Maybe           (MaybeT (..), runMaybeT)
import           Data.List.Extra                     (allSame, maximumOn)
import           Data.Map                            (Map)
import qualified Data.Map                            as Map
import           Data.Maybe                          (fromMaybe, mapMaybe)
import           Data.Time.Clock                     (UTCTime)
import qualified Data.Vector                         as V
import           Fmt                                 ((+||), (||+))
import           GHC.TypeNats                        (KnownNat)
import qualified Bootstrap.Math.AffineTransform  as AT
import qualified Hyperion.Bootstrap.PointCloudSearch as PC
import           Hyperion.Bootstrap.Qdelaunay        (qdelaunay)
import           Hyperion.Concurrent                 (Concurrently)
import qualified Hyperion.Database                   as DB
import           Hyperion.ExtVar                     (newExtVarStream)
import qualified Hyperion.Log                        as Log
import           Linear.Metric                       (distance)
import           Linear.V                            (Dim, V)
import qualified Linear.V                            as L
import           Linear.Vector                       (sumV, (^/))
import           Bootstrap.Math.Linear.Util               ()

data DelaunayConfig = DelaunayConfig
  { qdelaunayExecutable :: FilePath
  , simplexScore        :: SimplexScore
  , nThreads            :: Int
  , nSteps              :: Int
  , terminateTime       :: Maybe UTCTime
  } deriving (Eq, Ord, Show)

type Simplex n a = [ (V n Rational, a) ]

data SimplexScore
  = ChangeDistance
  | CandidateSeparation
  deriving (Eq, Ord, Show)

-- | Edges that connect vertices with different boolean values
changeEdges :: Simplex n Bool -> [(V n Rational, V n Rational)]
changeEdges simplex = do
  (p, b1) <- simplex
  (q, b2) <- simplex
  if b1 /= b2
    then return (p, q)
    else empty

-- | Minimum distance between vertices with different boolean
-- values. If all vertices have the same boolean values,
-- changeDistance is zero.
changeDistance :: Dim n => Simplex n Bool -> Double
changeDistance simplex =
  let es = changeEdges simplex
  in if es == []
     then 0
     else minimum [ distance (fmap fromRational p) (fmap fromRational q)
                  | (p, q) <- es ]

-- | Minimum distance between candidate point and other vertices. If
-- all vertices have the same boolean values, candidateSeparation is
-- zero.
candidateSeparation :: Dim n => Simplex n Bool -> Double
candidateSeparation simplex =
  let es = changeEdges simplex
  in if es == []
     then 0
     else
       let c = fmap fromRational (candidatePoint simplex)
       in minimum [distance (fmap fromRational p) c | (p,_) <- simplex]

-- | Do some vertices have different boolean values?
hasChange :: Simplex n Bool -> Bool
hasChange = not . allSame . map snd

-- | Average of the midpoints of the changeEdges
candidatePoint :: Dim n => Simplex n Bool -> V n Rational
candidatePoint simplex =
  let edges = changeEdges simplex
  in sumV [ p + q | (p, q) <- edges ] ^/ fromIntegral (2 * length edges)

-- | Maybe Bool is used to encode the status of the computation
-- involving each point. Nothing indicates the point is still being
-- computed. Just (True|False) indicates the computation has
-- completed. 'checkCompleted' checks if all the vertices have
-- completed and returns the simplex with its final boolean values.
checkCompleted :: Simplex n (Maybe Bool) -> Maybe (Simplex n Bool)
checkCompleted = traverse tupM
  where
    tupM (a, mb) = fmap (\b -> (a,b)) mb

-- | For each incomplete vertex, assume its result will be False.
assumeFalse :: Simplex n (Maybe Bool) -> Simplex n Bool
assumeFalse = map (second (fromMaybe False))

delaunaySearchPoint
  :: forall n . Dim n
  => DelaunayConfig
  -> Map (V n Rational) (Maybe Bool)
  -> IO (Maybe (V n Rational))
delaunaySearchPoint DelaunayConfig{..} pointMap = do
  indexedSimplices <- qdelaunay qdelaunayExecutable
                      (V.map (L.toVector . fmap fromRational) points)
  case filter hasChange (mapMaybe toSimplex indexedSimplices) of
    [] -> return Nothing
    searchSimplices -> do
      let score = case simplexScore of
            ChangeDistance      -> changeDistance
            CandidateSeparation -> candidateSeparation
      let s = maximumOn score searchSimplices
      Log.text $ "Delaunay search: "+||length searchSimplices||+
        " searchable / " +||length indexedSimplices||+" simplices"
      return (Just (candidatePoint s))
  where
    points = V.fromList (Map.keys pointMap)

    toSimplex :: V.Vector Int -> Maybe (Simplex n Bool)
    -- Speculate that uncompleted vertices will come back as false:
    -- toSimplex vertexIndices = Just $ assumeFalse $ do
    -- Don't do any speculation:
    toSimplex vertexIndices = checkCompleted $ do
      i <- V.toList vertexIndices
      let p = points V.! i
      return (p, pointMap Map.! p)

-- | Perform a Delaunay search over a region and persist the results
-- to a database.
--
-- In addition, we define an 'ExtVar' for a list of points [V n
-- Rational]. The 'ExtVar' is consulted *first* when determining the
-- next point to use. Thus, by inserting points into the 'ExtVar', the
-- user can choose the next points for the Delaunay search by hand
-- (pre-empting the use of Delaunay triangulation). Note that the
-- points in the 'ExtVar' are in the frame associated to the
-- 'AffineTransform' -- in other words, they are in the frame where
-- the allowed region should be roughly a sphere with O(1) radius.
--
-- To use the 'ExtVar', look for a line in the logs of the form
--
-- > [Fri 01/07/22 13:00:32] ExtVar for delaunaySearch: extVar ...
--
-- You can interact with the 'ExtVar' via GHCi -- see the
-- documentation in 'Hyperion.ExtVar' for details.
--
delaunaySearchRegionPersistent
  :: forall n m env .
     ( KnownNat n, MonadIO m, MonadReader env m, DB.HasDB env, MonadCatch m
     , Applicative (Concurrently m), MonadBase Process m
     )
  => DB.KeyValMap (V n Rational) (Maybe Bool)
  -> DelaunayConfig
  -> AT.AffineTransform n Rational
  -> Map (V n Rational) (Maybe Bool)
  -> (V n Rational -> m Bool)
  -> m PC.PointCloudStatus
delaunaySearchRegionPersistent results delaunayCfg affine initialPts f = do
  (eVar, getExtStream) <- newExtVarStream []
  Log.info "ExtVar for delaunaySearch" eVar
  let
    pointCloudCfg = PC.PointCloudConfig
      { PC.nThreads        = nThreads delaunayCfg
      , PC.nSteps          = const (nSteps delaunayCfg)
      , PC.getNextPoint    = \pointMap -> runMaybeT $
                                          MaybeT getExtStream <|>
                                          MaybeT (delaunaySearchPoint delaunayCfg pointMap)
      , PC.addBoundingCube = False
      , PC.terminateTime   = terminateTime delaunayCfg
      }
  PC.pointCloudSearchRegionPersistent results pointCloudCfg affine initialPts f

