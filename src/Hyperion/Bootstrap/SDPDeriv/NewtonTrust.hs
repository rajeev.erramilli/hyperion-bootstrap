{-# LANGUAGE DataKinds           #-}
{-# LANGUAGE DeriveAnyClass      #-}
{-# LANGUAGE DeriveGeneric       #-}
{-# LANGUAGE FlexibleContexts    #-}
{-# LANGUAGE FlexibleInstances   #-}
{-# LANGUAGE MultiWayIf          #-}
{-# LANGUAGE PolyKinds           #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE StaticPointers      #-}
{-# LANGUAGE TemplateHaskell     #-}
{-# LANGUAGE TypeApplications    #-}

module Hyperion.Bootstrap.SDPDeriv.NewtonTrust where

import           Bootstrap.Math.Linear              ((.*))
import qualified Bootstrap.Math.Linear              as L
import           Bootstrap.Math.Util                ((^))
import qualified Bootstrap.Math.VectorSpace         as VS
import           Control.Exception                  (Exception, throw)
import           Control.Monad.State                (evalStateT, get, modify)
import           Control.Monad.Trans                (lift)
import           Data.Binary                        (Binary)
import qualified Data.Foldable                      as Foldable
import           Data.Matrix.Static                 (Matrix)
import qualified Data.Matrix.Static                 as M
import           GHC.Generics                       (Generic)
import           GHC.TypeNats                       (KnownNat)
import           Hyperion.Bootstrap.SDPDeriv.Jet2   (Jet2 (..), eval,
                                                     newtonStep)
import           Hyperion.Bootstrap.SDPDeriv.Newton (approxRational')
import           Hyperion.Static                    (Dict (..), Static (..),
                                                     cAp, cPtr)
import           Hyperion.Static.Orphans            ()
import           Linear.Metric                      (norm)
import           Linear.V                           (V)
import           Numeric.Eigen.Static               (choleskyDecomposition,
                                                     eigensystemSymmetric)
import           Numeric.Rounded                    (Precision, Rounded,
                                                     Rounding,
                                                     RoundingMode (..))
import           Prelude                            hiding ((^))

type BigFloat p = Rounded 'TowardZero p

data Config n p = MkConfig
  { -- | Small deviation used when computing the gradient and hessian
    epsGradHess         :: Rational
    -- | Resolution used when converting numerical result for Newton
    -- step to Rational, and also used when performing a Newton search
    -- for the trust region step.
  , stepResolution      :: Rational
    -- | Terminate the Newton search when the step size gets smaller than this amount.
  , stepNormThreshold   :: BigFloat p
  , trustRegionMaxIters :: Int
  , trustRegionNorm     :: Matrix n n (BigFloat p)
  , eta1                :: BigFloat p
  , eta2                :: BigFloat p
  , eta3                :: BigFloat p
  , t1                  :: BigFloat p
  , t2                  :: BigFloat p
  , trustRadiusMax      :: BigFloat p
  } deriving (Eq, Ord, Show, Generic, Binary)

instance (KnownNat n, KnownNat p) => Static (Binary (Config n p)) where
  closureDict = cPtr (static (\Dict -> Dict)) `cAp` closureDict @(KnownNat n, KnownNat p)

-- | Parameters taken from https://optimization.mccormick.northwestern.edu/index.php/Trust-region_methods
defaultConfig :: KnownNat p => Matrix n n (BigFloat p) -> Config n p
defaultConfig n = MkConfig
  { epsGradHess         = 1e-9
  , stepResolution      = 1e-32
  , stepNormThreshold   = 1e-9
  , trustRegionMaxIters = 100
  , trustRegionNorm     = n
  , trustRadiusMax      = 100
  , eta1 = 0.2
  , eta2 = 0.25
  , eta3 = 0.75
  , t1   = 0.25
  , t2   = 2
  }

-- | Solves the problem
--
-- argmin_x (grad^T x + (1/2)* x^T hess x) such that |x| < radius
--
-- where |x| is the usual Euclidean norm, assuming the solution is on
-- the boundary of |x| < radius. This algorithm is described in
-- http://citeseerx.ist.psu.edu/viewdoc/summary?doi=10.1.1.716.2997
-- (pages 3 and 4).
trustRegionBoundaryStepEuclidean
  :: (Rounding r, Precision p, KnownNat n)
  => Rounded r p
  -> Int
  -> Rounded r p
  -> Jet2 n (Rounded r p)
  -> V n (Rounded r p)
trustRegionBoundaryStepEuclidean thresh maxIters radius jet = step
  where
    (eVals, eVecMatrix) = eigensystemSymmetric (hessian jet)
    eVecs = Foldable.toList (L.toCols eVecMatrix)
    mu_egs = do
      (mu, e) <- Foldable.toList eVals `zip` eVecs
      pure $ (mu, e .* gradient jet)
    lambda0 = Foldable.maximum $ 0 : [ abs eg/radius - mu | (mu,eg) <- mu_egs ]
    f  l = sum [    eg^2 / (mu+l)^2 | (mu,eg) <- mu_egs ] - radius^2
    f' l = sum [ -2*eg^2 / (mu+l)^3 | (mu,eg) <- mu_egs ]
    lambda = newtonSearch1d thresh maxIters f f' lambda0
    step = -VS.sum [ eg/(mu+lambda) VS.*^ e | ((mu,eg),e) <- zip mu_egs eVecs ]

data Newton1dMaxIterationsExceeded = Newton1dMaxIterationsExceeded
  deriving (Show, Exception)

-- | Perform a Newton's method search in 1-dimension. Needed for
-- trustRegionBoundaryStepEuclidean.
newtonSearch1d :: (Fractional a, Ord a) => a -> Int -> (a -> a) -> (a -> a) -> a -> a
newtonSearch1d thresh maxIters f f' x0 = go 1 x0
  where
    go n x
      | n > maxIters = throw Newton1dMaxIterationsExceeded
      | otherwise =
        let step = - f x / f' x
        in if abs step < thresh
           then x
           else go (n+1) (x+step)

-- | Solves the problem
--
-- argmin_x (grad^T x + (1/2)* x^T hess x) such that |x|_N < radius
--
-- where |x|_N^2 = x^T N x, assuming the solution is on the boundary
-- of |x|_N < radius.
trustRegionBoundaryStep
  :: (Rounding r, Precision p, KnownNat n)
  => Rounded r p
  -> Int
  -> Matrix n n (Rounded r p)
  -> Rounded r p
  -> Jet2 n (Rounded r p)
  -> V n (Rounded r p)
trustRegionBoundaryStep thresh maxIters trNorm radius jet = lTInv .* step
  where
    jet' = jet
      { hessian  = lInv .* hessian jet .* lTInv
      , gradient = lInv .* gradient jet
      }
    l = maybe (error "Norm not invertable") id (choleskyDecomposition trNorm)
    lInv  = either error id (M.inverse l)
    lTInv = M.transpose lInv
    step = trustRegionBoundaryStepEuclidean thresh maxIters radius jet'

data StepType = BoundaryStep | InteriorStep
  deriving (Eq, Ord, Show)

-- | Solves the problem
--
-- argmin_x (grad^T x + (1/2)* x^T hess x) such that |x|_N < radius
--
-- where |x|_N^2 = x^T N x. The solution is either a conventional
-- Newton step inside the trust region, or a boundary step.
trustRegionStep
  :: (Rounding r, Precision p, KnownNat n)
  => Rounded r p
  -> Int
  -> Matrix n n (Rounded r p)
  -> Rounded r p
  -> Jet2 n (Rounded r p)
  -> (StepType, V n (Rounded r p))
trustRegionStep thresh maxIters trNorm radius jet =
  let step0 = newtonStep jet
  in if step0 .* trNorm .* step0 < radius^2
     then (InteriorStep, step0)
     else (BoundaryStep, trustRegionBoundaryStep thresh maxIters trNorm radius jet)

-- | Given a monadic function getJet that takes the current number of
-- Newton steps and a point and returns a 'Jet2', perform a search
-- starting from 'xInitial' using Newton's method.
run
  :: (KnownNat n, KnownNat p, Monad m)
  => Config n p
  -> (Rational -> V n Rational -> m (Jet2 n (BigFloat p)))
  -> V n Rational
  -> m (V n Rational, [Jet2 n (BigFloat p)])
run cfg getJet xInitial = do
  jet <- getJet (epsGradHess cfg) xInitial
  evalStateT (go xInitial jet []) 1
  where
    -- | Get the step for the given jet, reading the trust radius from
    -- the current state
    getStep jet = do
      radius <- get
      pure $ trustRegionStep
        (fromRational (stepResolution cfg))
        (trustRegionMaxIters cfg)
        (trustRegionNorm cfg)
        radius
        jet

    -- | Rational approximation for a step
    approxStep = fmap (approxRational' (stepResolution cfg))

    -- | Compute a step and test the fidelity of the quadratic model
    -- at that step. Change the trust radius if necessary, and
    -- recursively re-compute the step if necessary.
    findStep x jet = do
      (stepStatus, step) <- getStep jet
      let x' = x + approxStep step
      jet' <- lift $ getJet (epsGradHess cfg) x'
      let
        fx  = eval jet  0
        fx' = eval jet' 0
        -- Fidelity of the quadratic model, called 'rho' in [1]
        fidelity = (fx - fx') / (fx - eval jet step)
      modify $ \radius -> if
        -- Decrease the trust region size
        | fidelity < eta2 cfg -> t1 cfg * radius
        -- Increase the trust region size
        | fidelity > eta3 cfg && stepStatus == BoundaryStep ->
          min (t2 cfg * radius) (trustRadiusMax cfg)
        | otherwise -> radius
      if
        -- Accept the step if the fidelity is sufficiently large
        -- OR if we decrease the objective. Note that the latter
        -- condition differs from [1]
        | fidelity > eta1 cfg || fx' < fx -> pure (x', jet')
        | otherwise                       -> findStep x jet

    -- | Find a valid step for the given jet. If the step satisfies
    -- the termination criterion, return the result; otherwise
    -- recurse.
    go x jet jets = do
      (x', jet') <- findStep x jet
      if
        | norm (fmap fromRational (x' - x)) < stepNormThreshold cfg ->
          pure (x, jet : jets)
        | otherwise ->
          go x' jet' (jet : jets)
