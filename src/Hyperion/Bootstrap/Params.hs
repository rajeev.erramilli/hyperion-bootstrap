{-# LANGUAGE DuplicateRecordFields #-}

-- | A collection of reasonable parameters for conformal bootstrap
-- computations, based on the value of nmax. These parameters are
-- mostly tested with IsingSigEps, and may need to be modified for
-- other problems.

module Hyperion.Bootstrap.Params where

import qualified Blocks.Blocks3d            as B3d
import           Blocks.ScalarBlocks        (ScalarBlockParams (..))
import           Bootstrap.Math.HalfInteger (HalfInteger)
import qualified SDPB


blockParamsNmax :: Int -> ScalarBlockParams
blockParamsNmax n
  | n <= 6 =
    ScalarBlockParams
    { nmax          = n
    , keptPoleOrder = 8
    , order         = 60
    , precision     = 768
    }
  | n <= 10 =
    ScalarBlockParams
    { nmax          = n
    , keptPoleOrder = 14
    , order         = 60
    , precision     = 768
    }
  | n <= 12 =
    ScalarBlockParams
    { nmax          = n
    , keptPoleOrder = 18
    , order         = 60
    , precision     = 768
    }
  | n <= 14 =
    ScalarBlockParams
    { nmax          = n
    , keptPoleOrder = 20
    , order         = 60
    , precision     = 768
    }
  | n <= 18 =
    ScalarBlockParams
    { nmax          = n
    , keptPoleOrder = 32
    , order         = 80
    , precision     = 960
    }
  | n <= 22 =
    ScalarBlockParams
    { nmax          = n
    , keptPoleOrder = 40
    , order         = 90
    , precision     = 1024
    }
    -- Not tested
  | n <= 26 =
    ScalarBlockParams
    { nmax          = n
    , keptPoleOrder = 48
    , order         = 100
    , precision     = 1068
    }
    -- Not tested
  | n <= 30 =
    ScalarBlockParams
    { nmax          = n
    , keptPoleOrder = 56
    , order         = 110
    , precision     = 1280
    }
  | otherwise = error ("No preset block table parameters for nmax " ++ show n)

block3dParamsNmax :: Int -> B3d.Block3dParams
block3dParamsNmax n
  | n <= 6 =
    B3d.Block3dParams
    { B3d.nmax          = n
    , B3d.keptPoleOrder = 8
    , B3d.order         = 60
    , B3d.precision     = 768
    }
  | n <= 10 =
    B3d.Block3dParams
    { B3d.nmax          = n
    , B3d.keptPoleOrder = 14
    , B3d.order         = 60
    , B3d.precision     = 768
    }
  | n <= 12 =
    B3d.Block3dParams
    { B3d.nmax          = n
    , B3d.keptPoleOrder = 18
    , B3d.order         = 60
    , B3d.precision     = 768
    }
  | n <= 14 =
    B3d.Block3dParams
    { B3d.nmax          = n
    , B3d.keptPoleOrder = 20
    , B3d.order         = 60
    , B3d.precision     = 768
    }
  | n <= 18 =
    B3d.Block3dParams
    { B3d.nmax          = 18
    , B3d.keptPoleOrder = 32
    , B3d.order         = 80
    , B3d.precision     = 960
    }
  | n <= 22 =
    B3d.Block3dParams
    { B3d.nmax          = 22
    , B3d.keptPoleOrder = 40
    , B3d.order         = 90
    , B3d.precision     = 1024
    }
    -- Not tested
  | n <= 26 =
    B3d.Block3dParams
    { B3d.nmax          = n
    , B3d.keptPoleOrder = 48
    , B3d.order         = 100
    , B3d.precision     = 1068
    }
    -- Not tested
  | n <= 30 =
    B3d.Block3dParams
    { B3d.nmax          = n
    , B3d.keptPoleOrder = 56
    , B3d.order         = 110
    , B3d.precision     = 1280
    }
  | otherwise = error ("No preset block table parameters for nmax " ++ show n)

spinsNmax :: Int -> [Int]
spinsNmax n
  | n <= 6 = [0 .. 21]
  | n <= 10 = [0 .. 26] ++ [49, 50]
  | n <= 12 = [0 .. 26] ++ [29, 30, 33, 34, 37, 38, 41, 42, 45, 46]
  | n <= 14 = [0 .. 26] ++ [29, 30, 33, 34, 37, 38, 41, 42, 45, 46, 49, 50]
  | n <= 18 = [0 .. 44] ++ [47, 48, 51, 52, 55, 56, 59, 60, 63, 64, 67, 68]
  | n <= 22 = [0 .. 64] ++ [67, 68, 71, 72, 75, 76, 79, 80, 83, 84, 87, 88]
  -- Untested
  | n <= 26 = [0 .. 84] ++ [87, 88, 91, 92, 95, 96, 99, 100, 103, 104, 107, 108]
  -- Untested
  | n <= 30 = [0 .. 104] ++ [107, 108, 111, 112, 115, 116, 119, 120, 123, 124, 127, 128]
  | otherwise = error ("No preset spin list for nmax " ++ show n)

gnyspinsNmax :: Int -> [HalfInteger]
gnyspinsNmax =
  addHalfSpins . spinsNmax
  where
    addHalfSpins xs = [ fromRational (dx + toRational x) | x <- xs, dx <- [0,1/2]]

sdpbParamsNmax :: Int -> SDPB.Params
sdpbParamsNmax n
  | n <= 6 =
    SDPB.defaultParams
    { SDPB.findPrimalFeasible       = True
    , SDPB.findDualFeasible         = True
    , SDPB.detectPrimalFeasibleJump = True
    , SDPB.detectDualFeasibleJump   = True
    }
  | n <= 10 =
    SDPB.defaultParams
    { SDPB.precision                = 448
    , SDPB.findPrimalFeasible       = True
    , SDPB.findDualFeasible         = True
    , SDPB.detectPrimalFeasibleJump = True
    , SDPB.detectDualFeasibleJump   = True
    , SDPB.initialMatrixScalePrimal = 1e40
    , SDPB.initialMatrixScaleDual   = 1e40
    , SDPB.maxRuntime               = SDPB.RunForDuration (4*3600)
    , SDPB.checkpointInterval       = 1800
    }
  | n <= 14 =
    SDPB.defaultParams
    { SDPB.precision                  = 640
    , SDPB.findPrimalFeasible         = True
    , SDPB.findDualFeasible           = True
    , SDPB.detectPrimalFeasibleJump   = True
    , SDPB.detectDualFeasibleJump     = True
    , SDPB.initialMatrixScalePrimal   = 1e50
    , SDPB.initialMatrixScaleDual     = 1e50
    , SDPB.maxComplementarity         = 1e130
    , SDPB.maxRuntime                 = SDPB.RunForDuration (4*3600)
    , SDPB.checkpointInterval         = 1800
    }
  | n <= 18 =
    SDPB.defaultParams
    { SDPB.precision                  = 768
    , SDPB.findPrimalFeasible         = True
    , SDPB.findDualFeasible           = True
    , SDPB.detectPrimalFeasibleJump   = True
    , SDPB.detectDualFeasibleJump     = True
    , SDPB.initialMatrixScalePrimal   = 1e50
    , SDPB.initialMatrixScaleDual     = 1e50
    , SDPB.maxComplementarity         = 1e160
    , SDPB.dualErrorThreshold         = 1e-40
    , SDPB.primalErrorThreshold       = 1e-40
    , SDPB.maxRuntime                 = SDPB.RunForDuration (8*3600)
    , SDPB.checkpointInterval         = 1800
    }
  | n <= 20 =
    SDPB.defaultParams
    { SDPB.precision                  = 896
    , SDPB.findPrimalFeasible         = True
    , SDPB.findDualFeasible           = True
    , SDPB.detectPrimalFeasibleJump   = True
    , SDPB.detectDualFeasibleJump     = True
    , SDPB.initialMatrixScalePrimal   = 1e60
    , SDPB.initialMatrixScaleDual     = 1e60
    , SDPB.maxComplementarity         = 1e180
    , SDPB.dualErrorThreshold         = 1e-70
    , SDPB.primalErrorThreshold       = 1e-70
    , SDPB.maxRuntime                 = SDPB.RunForDuration (8*3600)
    , SDPB.checkpointInterval         = 1800
    }
    -- Have not tested parameters for nmax > 22
  | otherwise =
    SDPB.defaultParams
    { SDPB.precision                  = 960
    , SDPB.findPrimalFeasible         = True
    , SDPB.findDualFeasible           = True
    , SDPB.detectPrimalFeasibleJump   = True
    , SDPB.detectDualFeasibleJump     = True
    , SDPB.initialMatrixScalePrimal   = 1e60
    , SDPB.initialMatrixScaleDual     = 1e60
    , SDPB.maxComplementarity         = 1e200
    , SDPB.dualErrorThreshold         = 1e-75
    , SDPB.primalErrorThreshold       = 1e-75
    , SDPB.dualityGapThreshold        = 1e-75
    , SDPB.maxRuntime                 = SDPB.RunForDuration (8*3600)
    , SDPB.checkpointInterval         = 1800
    }

jumpFindingParams :: Int -> SDPB.Params
jumpFindingParams n = (sdpbParamsNmax n)
  { SDPB.dualErrorThreshold       = 1e-200
  , SDPB.primalErrorThreshold     = 1e-200
  , SDPB.findDualFeasible         = False
  , SDPB.findPrimalFeasible       = False
  , SDPB.detectPrimalFeasibleJump = True
  , SDPB.detectDualFeasibleJump   = True
  }

optimizationParams :: Int -> SDPB.Params
optimizationParams n = (sdpbParamsNmax n)
  { SDPB.findPrimalFeasible       = False
  , SDPB.findDualFeasible         = False
  , SDPB.detectPrimalFeasibleJump = False
  , SDPB.detectDualFeasibleJump   = False
  }
