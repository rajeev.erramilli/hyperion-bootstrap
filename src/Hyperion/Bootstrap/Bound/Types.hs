{-# LANGUAGE ConstraintKinds           #-}
{-# LANGUAGE DataKinds                 #-}
{-# LANGUAGE DefaultSignatures         #-}
{-# LANGUAGE DeriveAnyClass            #-}
{-# LANGUAGE DeriveFunctor             #-}
{-# LANGUAGE DeriveGeneric             #-}
{-# LANGUAGE DuplicateRecordFields     #-}
{-# LANGUAGE ExistentialQuantification #-}
{-# LANGUAGE FlexibleContexts          #-}
{-# LANGUAGE FlexibleInstances         #-}
{-# LANGUAGE FunctionalDependencies    #-}
{-# LANGUAGE MultiParamTypeClasses     #-}
{-# LANGUAGE PolyKinds                 #-}
{-# LANGUAGE RankNTypes                #-}
{-# LANGUAGE ScopedTypeVariables       #-}
{-# LANGUAGE StaticPointers            #-}
{-# LANGUAGE TypeApplications          #-}
{-# LANGUAGE TypeFamilies              #-}
{-# LANGUAGE TypeOperators             #-}

module Hyperion.Bootstrap.Bound.Types where

import           Bootstrap.Build            (All, ComputeDependencies, DropVoid,
                                             FetchConfig, FetchT, FetchesAll,
                                             Keys, SomeBuildChain, Sum,
                                             SumDropVoid)
import           Bootstrap.Math.Linear.Util ()
import           Control.Monad.IO.Class     (MonadIO)
import           Data.Aeson                 (FromJSON, ToJSON)
import           Data.Binary                (Binary)
import           Data.Data                  (Typeable)
import           Data.Kind                  (Type)
import           Data.Proxy                 (Proxy)
import           Data.Reflection            (reifyNat)
import           GHC.Generics               (Generic)
import           GHC.TypeNats               (KnownNat, Nat)
import           Hyperion.Job               (Job)
import           Hyperion.Static            (Dict (..), Static (..), cAp, cPtr,
                                             ptrAp)
import           Numeric.Rounded            (Rounded, RoundingMode (TowardZero))
import           SDPB                       (SDP)
import qualified SDPB

data Bound prec a = Bound
  { boundKey     :: a
  , precision    :: prec
  , solverParams :: SDPB.Params
  , boundConfig  :: BoundConfig
  } deriving (Show, Eq, Ord, Generic, Binary, ToJSON, FromJSON, Functor)

instance (Typeable a, Typeable prec, Static (Binary a), Static (Binary prec)) =>
  Static (Binary (Bound prec a)) where
  closureDict = static (\Dict Dict -> Dict) `ptrAp` closureDict @(Binary a) `cAp` closureDict @(Binary prec)

data BoundDirection
  = UpperBound
  | LowerBound
  deriving (Show, Eq, Ord, Generic, Binary, ToJSON, FromJSON)

data BoundConfig = BoundConfig { scriptsDir :: FilePath }
  deriving (Show, Eq, Ord, Generic, Binary, ToJSON, FromJSON)

boundDirSign :: Num a => BoundDirection -> a
boundDirSign UpperBound = 1
boundDirSign LowerBound = -1

data BoundFiles = BoundFiles
  { jsonDir              :: FilePath
  , blockDir             :: FilePath
  , sdpDir               :: FilePath
  , outDir               :: FilePath
  , checkpointDir        :: FilePath
  , initialCheckpointDir :: Maybe FilePath
  } deriving (Show, Generic, Binary, ToJSON, FromJSON)

instance Static (Binary BoundFiles) where closureDict = cPtr (static Dict)

data FileTreatment = KeepFile | RemoveFile
  deriving (Eq, Ord, Show, Generic, Binary, ToJSON, FromJSON)

data BoundFileTreatment = MkBoundFileTreatment
  { jsonDirTreatment       :: FileTreatment
  , blockDirTreatment      :: FileTreatment
  , sdpDirTreatment        :: FileTreatment
  , outDirTreatment        :: FileTreatment
  , checkpointDirTreatment :: FileTreatment
  } deriving (Eq, Ord, Show, Generic, Binary, ToJSON, FromJSON)

instance Static (Binary BoundFileTreatment) where closureDict = cPtr (static Dict)

type family SDPFetchValue a k :: Type

type family ToKeyVals a ks :: [(Type, Type)] where
  ToKeyVals a '[] = '[]
  ToKeyVals a (k ': ks) = '(k, SDPFetchValue a k) ': (ToKeyVals a ks)

type BigFloat p = Rounded 'TowardZero p

type BoundKeyVals b p = ToKeyVals (BigFloat p) (SDPFetchKeys b)

type ComputeDependenciesContext b p =
  FetchesAll (BoundKeyVals b p) (ComputeDependencies (BoundKeyVals b p))

type FetchTContext (m :: Type -> Type) b p =
  FetchesAll (BoundKeyVals b p) (FetchT (BoundKeyVals b p) m)

type DropVoidContext b p =
  DropVoid (Sum (Keys (BoundKeyVals b p))) (SumDropVoid (SDPFetchKeys b))

type BoundFetchContext (m :: Type -> Type) b (p :: Nat) =
  ( KnownNat p
  , FetchTContext m b p
  , ComputeDependenciesContext b p
  , All Ord (Keys (BoundKeyVals b p))
  , DropVoidContext b p
  )

reifyNatFromInt ::  forall r. Int -> (forall (n :: Nat). KnownNat n => Proxy n -> r) -> r
reifyNatFromInt n = reifyNat (toInteger n)

class ToSDP b where
  type SDPFetchKeys b :: [Type]
  toSDP
    :: (KnownNat p, Applicative m, FetchesAll (BoundKeyVals b p) m)
    => b
    -> SDP m (BigFloat p)

  -- | Due to difficulties with type hackery, we provide a way to
  -- reify precision inside the typeclass.  There is a default
  -- implementation that works if there is only one block key.  If
  -- there is more than one block key, the method should simply be
  -- defined as
  --
  --     reifyPrecisionWithFetchContext _ _ n go = reifyNatFromInt n go
  --
  -- The issue is that the above code will always typecheck (provided
  -- block keys do not repeat), but GHC cannot derive that it will.
  --
  -- WARNING: As of "Simplified Subsumption" in GHC 9.0.2, you must
  -- use the eta-expanded definition of reifyPrecisionWithFetchContext
  -- given above. You cannot define it as
  --
  --     reifyPrecisionWithFetchContext _ _ = reifyNatFromInt **WRONG**
  --
  -- See
  -- https://www.reddit.com/r/haskell/comments/ujpzx3/was_simplified_subsumption_worth_it_for_industry/
  -- for a discussion of how simplified subsumption changed type
  -- inference involving constraints.
  reifyPrecisionWithFetchContext
    :: forall proxy proxy'' r m.
       proxy b
    -> proxy'' m
    -> Int
    -> (forall p . BoundFetchContext m b p => Proxy p -> r)
    -> r
  default reifyPrecisionWithFetchContext
    :: forall proxy proxy'' r m t. (SDPFetchKeys b ~ '[t], Ord t)
    => proxy b
    -> proxy'' m
    -> Int
    -> (forall p . BoundFetchContext m b p => Proxy p -> r)
    -> r
  reifyPrecisionWithFetchContext _ _ = reifyNatFromInt

-- | For some reason, we cannot use type equality directly inside a
-- 'Dict' when used with 'Static'. So for now, we need to provide this
-- class for use with Static.
class    (SDPDepBuildMonad b ~ Job) => BuildInJob b
instance (SDPDepBuildMonad b ~ Job) => BuildInJob b

-- | By default assume we build in Job.
class Ord (SumDropVoid (SDPFetchKeys b)) => SDPFetchBuildConfig b where
    type SDPDepBuildMonad b :: Type -> Type
    type SDPDepBuildMonad b = Job

    sdpFetchConfig :: (KnownNat p, MonadIO m)
      => proxy b
      -> proxy' p
      -> BoundFiles
      -> FetchConfig m (ToKeyVals (BigFloat p) (SDPFetchKeys b))

    sdpDepBuildChain
      :: proxy b
      -> BoundConfig
      -> BoundFiles
      -> SomeBuildChain (SDPDepBuildMonad b) (SumDropVoid (SDPFetchKeys b))
