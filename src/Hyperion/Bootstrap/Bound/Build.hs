{-# LANGUAGE DataKinds              #-}
{-# LANGUAGE FlexibleContexts       #-}
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE MultiParamTypeClasses  #-}
{-# LANGUAGE OverloadedStrings      #-}
{-# LANGUAGE PolyKinds              #-}
{-# LANGUAGE RankNTypes             #-}
{-# LANGUAGE ScopedTypeVariables    #-}
{-# LANGUAGE StaticPointers         #-}
{-# LANGUAGE TypeApplications       #-}
{-# LANGUAGE TypeFamilies           #-}
{-# LANGUAGE TypeOperators          #-}

module Hyperion.Bootstrap.Bound.Build where

import           Bootstrap.Build                (BuildLink (..),
                                                 ComputeDependencies, FetchT,
                                                 FetchesAll,
                                                 SomeBuildChain (..),
                                                 SumDropVoid, buildAndFetch,
                                                 buildSomeChain, dependencies,
                                                 runMemoFetchT, (<:<))
import           Control.Distributed.Process    (Process)
import qualified Control.Exception              as Exception
import           Control.Monad.Catch            (MonadCatch, handle)
import           Control.Monad.IO.Class         (MonadIO, liftIO)
import           Data.Binary                    (Binary)
import           Data.Functor.Identity          (Identity)
import           Data.Proxy                     (Proxy (..))
import qualified Data.Set                       as Set
import           Data.Tagged                    (Tagged, untag)
import qualified Data.Text                      as T
import           GHC.TypeNats                   (KnownNat, Nat, natVal)
import           Hyperion                       (Dict (..), HasWorkers,
                                                 Static (..), cAp, cPure, ptrAp,
                                                 remoteEval)
import           Hyperion.Bootstrap.Bound.Types (Bound (..), BoundFetchContext,
                                                 BoundFiles (..),
                                                 ComputeDependenciesContext,
                                                 DropVoidContext, FetchTContext,
                                                 SDPFetchBuildConfig (..),
                                                 ToKeyVals, ToSDP (..))
import           Hyperion.Concurrent            (Concurrently, mapConcurrently_)
import qualified Hyperion.Log                   as Log
import           Numeric.Rounded                (Rounded,
                                                 RoundingMode (TowardZero))
import qualified SDPB
import           System.Directory               (createDirectoryIfMissing,
                                                 doesPathExist)
import           Type.Reflection                (Typeable)

reifyBoundWithContext
    :: forall b r m . (ToSDP b)
    => Bound Int b
    -> (forall p . BoundFetchContext m b p => Bound (Proxy p) b -> r)
    -> Tagged m r
reifyBoundWithContext bound go = pure $
  reifyPrecisionWithFetchContext bound (Proxy @m) (precision bound) $ \proxy ->
  go (bound { precision = proxy })

reflectBound :: forall p b . KnownNat p => Bound (Proxy p) b -> Bound Int b
reflectBound bound = bound { precision = fromIntegral (natVal @p Proxy) }

boundToSDP
  :: ( ToSDP b
     , KnownNat p
     , Applicative f
     , FetchesAll (ToKeyVals (Rounded 'TowardZero p) (SDPFetchKeys b)) f
     )
  => Bound (Proxy p) b
  -> SDPB.SDP f (Rounded 'TowardZero p)
boundToSDP = toSDP . boundKey

withSDPDeps
  :: forall b r. (ToSDP b)
  => Bound Int b
  -> (forall (p :: Nat) . (KnownNat p, ComputeDependenciesContext b p, DropVoidContext b p)
      => SDPB.SDP
        (ComputeDependencies (ToKeyVals (Rounded 'TowardZero p) (SDPFetchKeys b))) (Rounded 'TowardZero p) -> r)
      -> r
withSDPDeps bound go = untag @Identity $
  reifyBoundWithContext bound (go . boundToSDP)

withSDPFetchT
  :: forall b r m proxy. (ToSDP b, Applicative m)
  => proxy m
  -> Bound Int b
  -> (forall (p :: Nat) . (KnownNat p, FetchTContext m b p, DropVoidContext b p)
      => SDPB.SDP (FetchT (ToKeyVals (Rounded 'TowardZero p) (SDPFetchKeys b)) m) (Rounded 'TowardZero p) -> r)
  -> r
withSDPFetchT _ bound go = untag @m $
  reifyBoundWithContext bound (go . boundToSDP)

boundBuildLink :: (MonadIO m, ToSDP b) => Bound Int b -> BuildLink m () SDPB.SDPPart
boundBuildLink bound =
  withSDPDeps bound $ \sdp -> BuildLink
  { buildDeps = const (SDPB.parts sdp)
  , checkCreated = const (pure False)
  , buildAll = const (pure ())
  }

sdpPartBuildLink
  :: ( Static (ToSDP b)
     , Static (SDPFetchBuildConfig b)
     , Static (Binary b)
     , Typeable b
     , HasWorkers m
     , Applicative (Concurrently m)
     )
  => Bound Int b
  -> BoundFiles
  -> BuildLink m SDPB.SDPPart (SumDropVoid (SDPFetchKeys b))
sdpPartBuildLink bound boundFiles =
  withSDPDeps bound $ \sdp -> BuildLink
  { buildDeps    = SDPB.withPart sdp dependencies
  , checkCreated = checkFilesCreated (pure . SDPB.partPath dir)
  , buildAll     = \parts -> do
      Log.text $ T.pack ("Building " ++ show (length parts) ++ " SDP parts")
      -- TODO: Can we have the build system automatically take care of directories?
      liftIO $ createDirectoryIfMissing True dir
      mapConcurrently_ (remoteWriteSDPPart bound boundFiles) (Set.toList parts)
      Log.text "Finished building SDP parts"
  }
  where
    dir = jsonDir boundFiles
    checkFilesCreated :: MonadIO m => (k -> [FilePath]) -> k -> m Bool
    checkFilesCreated toFiles key =
      liftIO $ and <$> mapM doesPathExist (toFiles key)

boundBuildChain
  :: ( Static (ToSDP b)
     , Static (SDPFetchBuildConfig b)
     , Static (Binary b)
     , Typeable b
     , HasWorkers m
     , Applicative (Concurrently m)
     , m ~ SDPDepBuildMonad b
     )
  => Bound Int b
  -> BoundFiles
  -> SomeBuildChain m ()
boundBuildChain bound boundFiles =
  boundBuildLink bound <:<
  sdpPartBuildLink bound boundFiles <:<
  sdpDepBuildChain bound (boundConfig bound) boundFiles

getBoundObject
  :: forall m b p r .
     ( MonadIO m
     , SDPFetchBuildConfig b
     , m ~ SDPDepBuildMonad b
     , BoundFetchContext m b p
     )
  => Bound (Proxy p) b
  -> BoundFiles
  -> (forall f . (Applicative f, FetchesAll (ToKeyVals (Rounded 'TowardZero p) (SDPFetchKeys b)) f) => b -> f r)
  -> m r
getBoundObject bound boundFiles toObject =
  buildAndFetch
    (sdpDepBuildChain (Proxy @b) (boundConfig bound) boundFiles)
    (sdpFetchConfig (Proxy @b) (Proxy @p) boundFiles)
    (toObject (boundKey bound))

-- | By default, we memoize the fetch function (which in general reads
-- and parses BlockTableKey's) in writeSDPPart. Note that
-- runMemoFetchT makes a memoization plan by determining dependencies
-- and then only keeping memoized results in memory as long as they
-- will be needed. Thus, unlike naive memoization, runMemoFetchT
-- shouldn't increase the memory usage too much unless many objects
-- are needed multiple times, with big overlaps between their time
-- intervals.
writeSDPPart
  :: forall b m. (ToSDP b, SDPFetchBuildConfig b, MonadIO m, MonadCatch m)
  => (Bound Int b, BoundFiles, SDPB.SDPPart)
  -> m ()
writeSDPPart (bound, boundFiles, sdpPart) =
  handle (Log.throw @m @Exception.SomeException) $ do
  untag @m $ reifyBoundWithContext bound $ \(bound' :: Bound (Proxy p) b) -> do
    Log.info "Writing" (SDPB.partPath (jsonDir boundFiles) sdpPart)
    let cfg = sdpFetchConfig (Proxy @b) (Proxy @p) boundFiles
    liftIO =<<
      runMemoFetchT
      (SDPB.encodePart (jsonDir boundFiles) sdpPart (boundToSDP bound'))
      cfg

remoteWriteSDPPart
  :: ( Static (ToSDP b)
     , Static (SDPFetchBuildConfig b)
     , Static (Binary b)
     , Typeable b
     , HasWorkers m
     )
  => Bound Int b
  -> BoundFiles
  -> SDPB.SDPPart
  -> m ()
remoteWriteSDPPart bound files sdpPart =
  remoteEval $
  static writePart `ptrAp` closureDict `cAp` cPure (bound, files, sdpPart)
  where
    writePart :: Dict (ToSDP b, SDPFetchBuildConfig b) -> (Bound Int b, BoundFiles, SDPB.SDPPart) -> Process ()
    writePart Dict args = writeSDPPart args

writeBoundToDirParallel
  :: ( HasWorkers m
     , Applicative (Concurrently m)
     , Typeable b
     , Static (ToSDP b)
     , Static (Binary b)
     , Static (SDPFetchBuildConfig b)
     , m ~ SDPDepBuildMonad b
     )
  => Bound Int b
  -> BoundFiles
  -> m [FilePath]
writeBoundToDirParallel b f = do
  let files = map (SDPB.partPath (jsonDir f)) (withSDPDeps b SDPB.parts)
  buildSomeChain (boundBuildChain b f) ()
  return files
